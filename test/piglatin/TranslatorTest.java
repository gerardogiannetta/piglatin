package piglatin;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonantK() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithConsonantHEndingWithVowelAndThanWithHAY() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithConsonantTEndingWithConsonantAndThanWithTAY() {
		String inputPhrase = "titanic";
		Translator translator = new Translator(inputPhrase);
		assertEquals("itanictay", translator.translate());
	}
	
	
	@Test
	public void testChangePositionOfFirstConsonantsToTheEndOfWord() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownkn", translator.changePositionOfFirstConsonants());
	}
	
	
	@Test
	public void testChangePositionOfFirstConsonantsToTheEndOfWordWithAWordOfOnlyConsonant() {
		String inputPhrase = "knwn";
		Translator translator = new Translator(inputPhrase);
		assertEquals("knwn", translator.changePositionOfFirstConsonants());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithTwoConsonantsKNAndEndingWithKNAYAndThanStartingWithO() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseWithMoreWords() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	
	@Test
	public void testTranslationCompositeWordsWith2DashBetween() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	
	@Test
	public void testTranslationCompositeWordsWith3DashBetween() {
		String inputPhrase = "well-being-world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay-orldway", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndWithExclamationMarkAtTheEnd() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndWithColonBetweenWords() {
		String inputPhrase = "hello: world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay: orldway", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithTwoConsonantsKNAndEndingWithKNAYAndThanStartingWithOAndEndingWithExclamationMark() {
		String inputPhrase = "known!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay!", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonantKAndThereAreComma() {
		String inputPhrase = "\"ask\"";
		Translator translator = new Translator(inputPhrase);
		assertEquals("\"askay\"", translator.translate());
	}
	
	
	@Test
	public void testTranslationPhraseStartingWithConsonantEndingWithConsonantAndThereIsApostropheBetweenWordsWithoutSpaces() {
		String inputPhrase = "there's";
		Translator translator = new Translator(inputPhrase);
		assertEquals("erethay'say", translator.translate());
	}
	
	
	@Test
	public void testTranslationOfComplexPhraseWithSpaceDashAndPunctuations() {
		String inputPhrase = "(there's) or-not!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(erethay'say) oray-otnay!", translator.translate());
	}

}
