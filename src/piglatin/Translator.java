package piglatin;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Translator {
	
	public static final String NIL = "nil";
	private String phrase;
	private Character[] myPunctuations = {'.', '"', '\'', ';', ':', '!', '?', '(', ')'};

	public Translator(String inputPhrase) {
		this.phrase = inputPhrase;
		this.phrase = this.phrase.trim();
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		if (! phrase.isEmpty()) {
			String oldPhrase = phrase;
			
			List<String> words = getToken(" ");
			List<String> wordsWithDash = getToken("-");
		    
		    if (words.size() > 1) {
		    	String result = translateMoreWordsWithSpace(words, " ");
		    	phrase = oldPhrase;
		    	return result;
		    }
		    else if (wordsWithDash.size() > 1) {
		    	String result = translateMoreWordsWithDash(wordsWithDash, "-");
		    	phrase = oldPhrase;
		    	return result;
		    }
		    else if (startWithVowel(phrase)) {
		    	return getPhraseWithTheNewEndAndPunctuations();
			}
			else if (startWithConsonant(phrase)) {
				return changePositionOfFirstConsonantsAndPunctuations();
			}
			else if (wordStartsWithPunctuations(phrase)) {
				if (startWithVowel(phrase.substring(1, phrase.length()-1))) {
					return getPhraseWithTheNewEndAndPunctuations();
				}
				else {
					return changePositionOfFirstConsonantsAndPunctuations();
				}
			}
		}
		return NIL;
	}
	
	
	private String getPhraseWithTheNewEnd() {
		if (phrase.endsWith("y")) {
			return phrase + "nay";
		}
		else if (endWithVowel()) {
			return phrase + "yay";
		}
		else if (endWithConsonant()) {
			return phrase + "ay";
		}
		return NIL;
	}
	
	
	private String getPhraseWithTheNewEndAndPunctuations() {
		ArrayList<Object> arr = findPunctuations();
		Boolean thereArePunctuations = (Boolean)arr.get(0);
		Character cStart = (Character)arr.get(1);
		Character cEnd = (Character)arr.get(2);
		String res = getPhraseWithTheNewEnd();
		if (Boolean.TRUE.equals(thereArePunctuations)) {
			return addPunctuations(cStart, cEnd, res);
		}
		return res;
	}
	
	
	private String changePositionOfFirstConsonantsAndPunctuations() {
		ArrayList<Object> arr = findPunctuations();
		Boolean thereArePunctuations = (Boolean)arr.get(0);
		Character cStart = (Character)arr.get(1);
		Character cEnd = (Character)arr.get(2);
		String res = changePositionOfFirstConsonants() + "ay";
		if (Boolean.TRUE.equals(thereArePunctuations)) {
			return addPunctuations(cStart, cEnd, res);
		}
		return res;
	}
	
	
	private String addPunctuations(Character cStart, Character cEnd, String res) {
		if (cStart != null) {
			res = cStart + res;
		}
		if (cEnd != null) {
			res += cEnd;
		}
		return res;
	}
	
	
	private ArrayList<Object> findPunctuations() {
		Character cStart = null;
		Character cEnd = null;
		Boolean thereArePunctuations = false;
		if (wordStartsWithPunctuations(phrase)) {
			cStart = phrase.charAt(0);
			phrase = phrase.substring(1);
			thereArePunctuations = true;
		}
		if (wordEndsWithPunctuations(phrase)) {
			cEnd = phrase.charAt(phrase.length()-1);
			phrase = phrase.substring(0, phrase.length()-1);
			thereArePunctuations = true;
		}
		ArrayList<Object> arr = new ArrayList<>();
		arr.add(thereArePunctuations);
		arr.add(cStart);
		arr.add(cEnd);
		return arr;
	}
	
	
	private List<String> getToken(String delim) {
		StringTokenizer st = new StringTokenizer(phrase, delim);
		List<String> words = new LinkedList<>();
	    while (st.hasMoreTokens()) {
	    	words.add(st.nextToken());
	    }
	    return words;
	}
	
	
	private String translateMoreWordsWithSpace(List<String> words, String delim) {
		return translateMoreWords(words, delim).trim();
	}
	
	
	private String translateMoreWordsWithDash(List<String> words, String delim) {
    	String result = translateMoreWords(words, delim);
    	return result.substring(0, result.length()-1);
	}
	
	
	private String translateMoreWords(List<String> words, String delim) {
		StringBuilder rst = new StringBuilder();
    	for (String s : words) {
			phrase = s;
			rst.append(translate() + delim);
		}
    	return rst.toString();
	}
	
	
	private boolean wordStartsWithPunctuations(String string) {
		return ((string.charAt(0) == '.')   || 
				(string.charAt(0) == '"')   || 
				(string.charAt(0) == ';')   || 
				(string.charAt(0) == ':')   || 
				(string.charAt(0) == '?')   || 
				(string.charAt(0) == '!')   || 
				(string.charAt(0) == '\'')  ||
				(string.charAt(0) == '(')   || 
				(string.charAt(0) == ')')); 
	}
	
	
	private boolean wordEndsWithPunctuations(String string) {
		int end = string.length()-1;
		return ((string.charAt(end) == '.')   || 
				(string.charAt(end) == '"')   || 
				(string.charAt(end) == ';')   || 
				(string.charAt(end) == ':')   || 
				(string.charAt(end) == '?')   || 
				(string.charAt(end) == '!')   || 
				(string.charAt(end) == '\'')  ||
				(string.charAt(end) == '(')   || 
				(string.charAt(end) == ')')); 
	}
	
	
	private boolean startWithVowel(String string) {
		return string.startsWith("a")  || 
				string.startsWith("e") || 
				string.startsWith("i") || 
				string.startsWith("o") ||
				string.startsWith("u");
	}
	
	
	private boolean startWithConsonant(String string) {
		return regexConsonant(string.substring(0, 1));
	}
	
	
	private boolean endWithVowel() {
		return phrase.endsWith("a")  || 
				phrase.endsWith("e") || 
				phrase.endsWith("i") || 
				phrase.endsWith("o") ||
				phrase.endsWith("u");
	}
	
	
	private boolean endWithConsonant() {
		return regexConsonant(phrase.substring(phrase.length() - 1));
	}
	
	
	private boolean regexConsonant(String character) {
		Pattern p = Pattern.compile("[\\p{Alpha}&&[^aeiou]]");
		Matcher m = p.matcher(character);
		return m.matches();
	}

	
	public String changePositionOfFirstConsonants() {
		
		String firstRes = recursiveTranslateForPunctuations();
		if (firstRes != null) return firstRes;
		
		StringBuilder str = new StringBuilder();
		String newPhrase = "";
		int lastIndex = phrase.length()-1;
		int i = 1;
		
		while (i <= lastIndex) {
			String subString = phrase.substring(i-1, i);
			if (! regexConsonant(subString)) break;
			str.append(subString);
			newPhrase = phrase.substring(i);
			i++;
		}
		
		if ((i-1) == lastIndex) {
			String finalSubString = phrase.substring(lastIndex);
			if (regexConsonant(finalSubString)) {
				str.append(finalSubString);
				newPhrase = "";
			}
		}
		
		return newPhrase + str.toString();
	}
	
	
	private String recursiveTranslateForPunctuations() {
		for (Character c : myPunctuations) {
			List<String> punctuationStrings = getToken(""+c);
			if (punctuationStrings.size() > 1) {
				Character charFound = c;
				String oldPhrase = phrase;
				StringBuilder newResult = new StringBuilder();
				for (String s : punctuationStrings) {
					phrase = s;
					newResult.append(translate()+charFound);
				}
				phrase = oldPhrase;
				String res = newResult.toString();
				return res.substring(0, res.length()-3);
			}
		}
		return null;
	}

}
